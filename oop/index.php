<?php
    require_once('Animal.php');
    require_once('Frog.php');
    require_once('Ape.php');
    
$sheep = new Animal("shaun");

echo "Nama Hewan : ". $sheep->name . "<br>"; // "shaun"
echo "Berkaki : ".$sheep->legs ."<br>"; // 4
echo "Berdarah dingin : " . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new Frog("buduk");
echo "Nama Hewan : ". $kodok->name . "<br>";
echo "Berkaki : " . $kodok ->legs . "<br>";
echo "Berdarah dingin : " . $kodok->cold_blooded ."<br>";
echo "Jump : ". $kodok->jump() . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewa : " . $sungokong->name . "<br>";
echo "Berkaki : " . $sungokong ->legs . "<br>";
echo "Berdarah dingin : " . $sungokong ->cold_blooded ."<br>";
echo "Yell : " . $sungokong->yell() // "Auooo"
 

?>